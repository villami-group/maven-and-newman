package com.ibm.my_micro;

import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("v1")
public class My_microApplication extends Application {
}