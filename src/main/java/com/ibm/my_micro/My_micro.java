package com.ibm.my_micro;

import javax.enterprise.context.ApplicationScoped;

// http://localhost:3000/openapi/ui/

// JSON
import javax.json.Json;
import javax.json.JsonObject;

// OPEN API
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;


// JAX-RS
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;


@ApplicationScoped
@Path("/product")
@OpenAPIDefinition(info = @Info(title = "my_micro Service", version = "1.0", description = "my_micro Service APIs"))
public class My_micro {
	
	Product array[] = new Product[10];

	public static void main( String args[] ) { 
	}

	@Path("/get/{id}")
	@GET
	public Response getProduct(@PathParam("id") Integer id) {
		System.out.println("/get invoked");
		System.out.println(id);
		if(this.array[id] == null ){
			return Response.noContent().build();
		}else{
			Product product = this.array[id];
			return Response.ok(this.createJson(product)).build();
		}
	}


	@Path("/post")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postProduct(@RequestBody Product product) {
		System.out.println("/post invoked");
		System.out.println(product.name);

		this.array[product.id] = product;

		return Response.ok(this.createJson(product)).build();
	}

	@Path("/put")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putProduct(@RequestBody Product product) {
		System.out.println("/put invoked");
		System.out.println(product.name);

		if(this.array[product.id] == null ){
			return Response.noContent().build();
		}else{
			this.array[product.id] = product;
			return Response.ok(this.createJson(product)).build();
		}
	}

	@Path("/delete/{id}")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteProduct(@PathParam("id") Integer id) {
		System.out.println("/delete invoked");
		System.out.println(id);

		if(this.array[id] == null ){
			return Response.noContent().build();
		}else{
			Product product = this.array[id];
			this.array[id] = null;
			return Response.ok(this.createJson(product)).build();
		}
	}


	private JsonObject createJson(Product product) {
		JsonObject output = Json.createObjectBuilder().add("id",product.id).add("name", product.name).add("price", product.price)
				.add("description", product.description).build();
		return output;
	}
}